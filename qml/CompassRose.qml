import QtQuick 2.7
Image{
    id: rose
    anchors.left: parent.left
    anchors.top: parent.top
    anchors.margins: units.gu(2)
    width:units.gu(5)
    height: width
    source: "../assets/indicator.svg" // "rose.webp"
}
