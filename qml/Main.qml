/*
 * Copyright (C) 2019  Peter Putz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtLocation 5.6

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'suncompass.doniks'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    SCState {
        id: scstate
        latitude: map.center.latitude
        longitude: map.center.longitude
    }


    // rate at which the clock runs in [seconds per second]
    // 1.0 means normal speed
    property real clockRate: 1.0
    // freeze the clock
    property bool clockFreeze: false


    function deg2rad(deg){
        return deg / 180 * Math.PI
    }

    function rad2deg(rad){
        return rad * 180 / Math.PI
    }


    Settings {
        id: settings
        property alias width: root.width
        property alias height: root.height
        property real centerLat: 0.0
        property real centerLon: 0.0
        property int zoomLevel: 1
        //property alias sunAngle: rot.angle
        //property alias rotating: rotationTimer.running
        property alias debuggr: debuggr.show
        property alias datetime: datetimeconfig.visible
        property alias freeze: root.clockFreeze
        property alias date: scstate.date
        property alias rate: root.clockRate
    }


    function logTime() {
        console.log("scstate.date", scstate.date)
        console.log("root.clockFreeze", root.clockFreeze)
        console.log("root.clockRate", root.clockRate, "[s/s] = ", clock.delta, "/", clock.interval, " [ms/ms]")
    }

    PageStack {
        id: pageStack

        Page {
            id: page
            anchors.fill: parent

            header: PageHeader {
                id: header
                title: i18n.tr('Sun Compass')
                trailingActionBar {
                    numberOfSlots: 3
                    actions: [
                        Action {
                            id: settingsAction
                            iconName: "settings"
                            text: i18n.tr("Settings")
                            onTriggered: {
                                console.log("settings")
                                debuggr.show = ! debuggr.show
                            }
                        },

                        Action {
                            id: centerPosAction
                            iconName: "gps"
                            text: i18n.tr("Center on Position")
                            onTriggered: {
                                console.log("center")
                            }
                        },

                        Action {
                            id: dateAction
                            iconName: "calendar"
                            text: i18n.tr("Set date and time")
                            onTriggered: {
                                console.log("calendar")
                                // onClicked: pageStack.push(Qt.resolvedUrl("PageDate.qml"))
                                datetimeconfig.visible = ! datetimeconfig.visible
                            }
                        }
                    ]
                }
            }

            Item {
                id: main
                anchors {
                    top: header.bottom
                    bottom: page.bottom
                    left: page.left
                    right: page.right
                }
                property double centerY: height/2
                property double centerX: width/2


                Plugin {
                    id: mapPlugin
                    name: "osm" // "mapboxgl", "esri", ...
                }
                MyPositionSource {
                    id: positionSource
                }
                MyMap{
                    id: map
                }
                CompassRose {}


                Debuggr {
                    id: debuggr
                    x: margin
                    y: margin
                    // with show, we *request* to show it
                    // however if the datetimeconfig is visible and covering the whole screen
                    // then don't make debuggr visible
                    property bool show: false
                    visible: show && ! ( datetimeconfig.visible && datetimeconfig.fillParent )
                }

                DateTimeInlay {
                    id: datetimeconfig
                    anchors.left: parent.left
                    anchors.leftMargin: margin
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: bMargin
                }
            }
        }

        Component.onCompleted: {
            push(page)
        }

        Component.onDestruction: {
            // write settings back
            settings.centerLat = scstate.latitude
            settings.centerLon = scstate.longitude
            settings.zoomLevel = map.zoomLevel
        }

        Timer {
            id: clock
            running: !root.clockFreeze
            repeat: running
            interval: 1000 // [milliseconds]
            // delta specifies how many milliseconds we should increment every
            // time the Timer triggers, we derive it from rate and interval
            //   delta           [milliseconds/interval]
            //   root.clockRate  [seconds/second]
            // ie, delta = root.clockRate * ( interval / 1000.0 ) * 1000.0
            // or simplified:
            property real delta: root.clockRate * interval
            onTriggered: {
                if (clockRate === 1.0) {
                    scstate.date = new Date()
                } else {
                    // increment the milliseconds
                    scstate.date = new Date( scstate.date.getTime() + delta )
                }
                //logTime()
            }
        }
    }
}
